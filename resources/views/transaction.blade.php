@extends('layouts.head')

@section('content')

            <div class="row" ng-app="transApp" ng-controller="transCtrl">
                <div class="col-lg-12">
                  <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit Card Information
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="dataTable_wrapper">
                                <table class="table table-striped table-bordered table-hover" id="dataTables-example">
                                    <thead>
                                        <tr>
                                            <th>Transaction No.</th>
                                            <th>User ID</th>
                                            <th>Date</th>
                                            <th>Seller No.</th>
                                            <th>Product</th>
                                            <th>Price</th>
                                            <th>Credit Card No.</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr class="odd gradeX" ng-repeat="d in data">
                                                                                        <td>@{{d.transno}}</td>
                                                                                        <td>@{{d.uid}}</td>
                                                                                        <td>@{{d.date}}</td>
                                                                                        <td>@{{d.sellerno}}</td>
                                                                                        <td>@{{d.product}}</td>
                                                                                        <td>@{{d.price}}</td>
                                                                                        <td>@{{d.number}}</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                            
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                
                <!-- /.col-lg-8 -->
                
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
            <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script type="text/javascript" src="/js/transactions.js"></script>
@endsection