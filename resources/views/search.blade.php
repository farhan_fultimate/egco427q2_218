@extends('layouts.head')

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            List of News
                        </div>
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-lg-12">
                                        <h2>Search Links</h2>
                                                <form>
                                                <select onchange="showRSS(this.value)">
                                                <option value="">Select an RSS-feed:</option>
                                                <option value="Google">Google News</option>
                                                <option value="Sanook">Sanook IT News</option>
                                                </select>
                                                </form>
                                                <br>
                                                <div id="rssOutput">RSS-feed will be listed here...</div>
                                </div>
                            </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <!-- /.row -->
            
            <!-- /.row -->
            <div class="row">
                
                <!-- /.col-lg-8 -->
                
                <!-- /.col-lg-4 -->
            </div>
            <!-- /.row -->
            <script>
        function showRSS(str)
        {
        if (str.length==0)
          {
          document.getElementById("rssOutput").innerHTML="";
          return;
          }
        if (window.XMLHttpRequest){
          xmlhttp=new XMLHttpRequest();
        }
        else{
          xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
        }
        xmlhttp.onreadystatechange=function(){
          if (xmlhttp.readyState==4 && xmlhttp.status==200)
            {
            document.getElementById("rssOutput").innerHTML=xmlhttp.responseText;
            }
        }
        xmlhttp.open("GET","getrss.php?q="+str,true);
        xmlhttp.send();
        }


    </script>
@endsection