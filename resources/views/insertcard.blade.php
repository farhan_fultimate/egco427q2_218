@extends('layouts.head')

@section('content')
            <div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="color: ">Credit Card Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <div class="col-lg-6" ng-app="myApp" ng-controller="myCtrl">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Insert credit Card Transaction
                        </div>
                        <div class="panel-body ng-scope">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" name="card" method="post" class="ng-pristine ng-valid" ng-app="myApp" ng-controller="myCtrl">
                                     
                                        <div class="form-group">
                                            <label>Transaction No.</label>
                                            <input name="transno" class="form-control ng-pristine ng-untouched ng-valid" placeholder="" ng-model="transno" disabled="true">
                                            
                                        
                                        </div>

                                        <label>User ID No.</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input name="uid" type="number" class="form-control ng-pristine ng-untouched ng-valid" placeholder="User ####" ng-model="uid" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Credit Card No.</label>
                                            <input class="form-control ng-pristine ng-untouched ng-valid" placeholder="Enter Creditcard No." ng-model="number" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="date" class="form-control ng-pristine ng-untouched ng-valid" placeholder="Enter Date" ng-model="date" required>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Seller No.</label>
                                            <input id="sellerno" name="sellerno" class="form-control ng-pristine ng-untouched ng-valid" placeholder="Enter Seller No." ng-model="sell" required>
                                        </div>

                                        <div class="form-group">
                                            <label>Product</label>
                                            <input id="product" name="product" class="form-control ng-pristine ng-untouched ng-valid" placeholder="Enter Product" ng-model="product" required >
                                        </div>

                                        <label>Price</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="price" name="price" type="text" class="form-control ng-pristine ng-untouched ng-valid" ng-model="price" required>
                                            <span class="input-group-addon">.00</span>
                                        </div>
                                        <table width="100%"><tbody><tr>
                                        <td width="30%"></td>
                                        
                                        <td width="40%">    
                                                <button id="save" name="save" ng-click="sendData()" type="submit" class="btn btn-success btn-circle btn-lg" ><i class="fa fa-save"></i></button>
                                                <button type="button" class="btn btn-danger btn-circle btn-lg" onclick="location.href='/cardstatement'"><i class="fa fa-remove"></i></button>
                                        </td>
                                        <td width="10%"></td>
                                        </tr></tbody></table>
                                        <br>
                                        <div class="alert alert-danger ng-binding ng-hide" role="alert" ng-show="max_alert">
                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                          <span class="sr-only">Error:</span>
                                        </div>
                                        <div class="alert alert-success ng-binding ng-hide" role="alert" ng-show="success">
                                            @{{alert_info}}
                                        </div>
                                    </form>
                                   </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
            <!-- /.row -->
            <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

    <script>
        var app = angular.module('myApp', []);
        app.controller('myCtrl' , function($scope , $http){
            $scope.sendData = function(){
                var d = new Date($scope.date);
                var datestring =d.getFullYear()+"-"+ d.getDate()  + "-" + (d.getMonth()+1);
                var data = $.param({
                        price: $scope.price,
                        date: datestring,
                        sell: $scope.sell,
                        product: $scope.product,
                        number: $scope.number,
                        uid: $scope.uid
                });
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
               $http.post('/cardinsert', data, config)
                   .then(
                       function(data, status, headers, config ){
                            $scope.success = true;
                            $scope.alert_info = data.data;
                    
                       },   
                       function(data, status, headers, config){
                         
                       }
                    );
            }
        });
    </script>
@endsection