@extends('layouts.head')

@section('content')
<div class="row">
                <div class="col-lg-12">
                    <h1 class="page-header" style="color: #33ccff">Credit Card Statement</h1>
                </div>
                <!-- /.col-lg-12 -->
            </div>
           
            <div class="row">
                    <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Credit Card Transaction
                        </div>
                        <div class="panel-body" ng-app="myApp" ng-controller="myCtrl">
                            <div class="row">
                                <div class="col-lg-12">
                                    <form role="form" name="card" action="savecard.php" method="post">
                                     
                                        <div class="form-group" >
                                            <label>Transaction No.</label>
                                            <input name="transno" class="form-control" placeholder="Enter text" ng-model="transno" readonly>
                                            
                                        
                                        </div>

                                        <label>User ID No.</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">@</span>
                                            <input name="uid" type="text" class="form-control" placeholder="User ####" ng-model="uid" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Credit Card No.</label>
                                            <input class="form-control" placeholder="Enter Creditcard No." ng-model="number" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Date</label>
                                            <input type="date" class="form-control" placeholder="Enter Date" ng-model="date" readonly>
                                        </div>
                                    
                                        <div class="form-group">
                                            <label>Seller No.</label>
                                            <input id="sellerno" name="sellerno" class="form-control" placeholder="Enter Seller No." ng-model="sell" readonly>
                                        </div>

                                        <div class="form-group">
                                            <label>Product</label>
                                            <input id="product" name="product" class="form-control" placeholder="Enter Product" ng-model="product" readonly >
                                        </div>

                                        <label>Price</label>
                                        <div class="form-group input-group">
                                            <span class="input-group-addon">$</span>
                                            <input id="price" name="price" type="text" class="form-control" ng-model="price" readonly>
                                            <span class="input-group-addon">.00</span>
                                        </div>
                                        <table width="100%"><tbody><tr>
                                        <td width="25%"><button id="backward" type="button" class="btn btn-default btn-circle btn-lg" ng-click="backward()"><i class="fa fa-angle-left"></i></button></td>
                                        
                                        <td width="50%">    
                                                <button type="button" class="btn btn-primary btn-circle btn-lg" onclick="editbox()"><i class="fa fa-edit"></i></button>
                                                <button id="save" name="save" type="submit" class="btn btn-success btn-circle btn-lg" ng-click="sendData()" disabled=""><i class="fa fa-save"></i></button>
                                                <button type="button" class="btn btn-warning btn-circle btn-lg" onclick="location.href='/insertcard'"><i class="fa fa-plus"></i></button>
                                        </td>
                                        <td width="25%"><button id="forward" type="button" class="btn btn-default btn-circle btn-lg" ng-click="forward()"><i class="fa fa-angle-right"></i></button></td>
                                        </tr></tbody></table>
                                        <br>
                                        <div class="alert alert-danger" role="alert" ng-show="max_alert">
                                          <span class="glyphicon glyphicon-exclamation-sign" aria-hidden="true"></span>
                                          <span class="sr-only">Error:</span>@{{alert_info}}
                                        </div>
                                        <div class="alert alert-success" role="alert" ng-show="success">@{{alert_info}}

                                        </div>
                                    </form>
                                   </div>
                                </div>
                                <!-- /.col-lg-6 (nested) -->
                            
                            <!-- /.row (nested) -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-12 -->
            </div>
            </div>
            <!-- /.row -->
            <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>

    <script type="text/javascript">

        function editbox(){
            document.getElementById('sellerno').readOnly=!document.getElementById('sellerno').readOnly;
            document.getElementById('product').readOnly= !document.getElementById('product').readOnly;
            document.getElementById('price').readOnly=!document.getElementById('price').readOnly;
            document.getElementById('save').disabled=!document.getElementById('save').disabled;
            document.getElementById('forward').disabled=!document.getElementById('save').disabled;
            document.getElementById('backward').disabled=!document.getElementById('save').disabled;
        }
        var app = angular.module("myApp" , []);
        app.controller('myCtrl' , function($scope , $http){
            $scope.index = 1;
            $scope.max = false;
            $scope.max_alert = false;
            var loadData = function(){
                $http.get('/getstatement/'+$scope.index).then(function(respond){
                     $scope.myData = respond.data;
                    if($scope.myData[0].val == '0'){
                        $scope.index--;
                        $scope.max = true;
                        $scope.max_alert = true;
                        $scope.alert_info = "No data for next record"
                        return false;
                    }
                    $scope.price = $scope.myData[0].price;
                    $scope.date = new Date($scope.myData[0].date);
                    $scope.sell = $scope.myData[0].sellerno;
                    $scope.product = $scope.myData[0].product;
                    $scope.number = $scope.myData[0].number;
                    $scope.transno = $scope.myData[0].transno;
                    $scope.uid = $scope.myData[0].uid;  
                    return true;
                });
            }
            var saveData = function(){

            }
            $scope.forward = function(){
                 if(!$scope.max){
                    $scope.index++;
                    $scope.success = false;
                    $scope.max_alert = false;
                    loadData();
                }
            }
            $scope.backward = function() {
                $scope.index--;
                $scope.success = false;
                $scope.max_alert = false;
                if($scope.index <=1 ) $scope.index = 1;
                if($scope.max == true) {
                    $scope.max = false;
                    $scope.max_alert = false;   
                }
                loadData();
            }
            loadData();
            $scope.sendData = function(){
                editbox();
                var data = $.param({
                        'price' : $scope.price,
                        'sell' : $scope.sell,
                        'product' : $scope.product,
                        'transno' : $scope.transno
                });
                var config = {
                    headers : {
                        'Content-Type': 'application/x-www-form-urlencoded;charset=utf-8;'
                    }
                }
               $http.post('/cardupdate', data, config)
                   .then(
                       function(data, status, headers, config ){
                         if(data.data == "success"){
                            $scope.success = true;
                            $scope.alert_info = "success";
                         } 
                         else{
                            $scope.max_alert = true;
                            $scope.alert_info = "fail";
                               
                         }
                       },   
                       function(data, status, headers, config){
                         
                       }
                    );
            }
        });

    </script>
@endsection