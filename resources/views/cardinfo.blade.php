@extends('layouts.head')

@section('content')
            <div class="row" ng-app="cardInfoApp">
                <div class="col-lg-12">
                    <div class="panel panel-default">
                        <div class="panel-body">
                            <div class="panel panel-default">
                                <div class="panel-heading">
                                    <h3 class="panel-title">Credit Card Information</h3>
                                </div>
                                <div class="panel-body" ng-controller="cardInfoCtrl">
                                    <table class="table table-hover">
                                        <thead>
                                            <tr>
                                                <th>Name</th>
                                                <th>Number</th>
                                                <th>Issuer</th>
                                                <th>Expire</th>
                                                <th>Limit</th>
                                                <th>Currency</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr ng-repeat="d in data">
                                                <th>@{{d.name}}</th>
                                                <th>@{{d.number}}</th>
                                                <th>@{{d.issuer}}</th>
                                                <th>@{{d.exp}}</th>
                                                <th>@{{d.limit}}</th>
                                                <th>@{{d.currency}}</th>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- /.col-lg-12 -->
            </div>
            <script src="http://ajax.googleapis.com/ajax/libs/angularjs/1.4.8/angular.min.js"></script>
    <script type="text/javascript" src="/js/cardinfo.js"></script>
@endsection