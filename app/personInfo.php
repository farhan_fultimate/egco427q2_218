<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class personInfo extends Model
{
    //
    protected $table = 'personinfo';
    protected $primarykey = 'id';
}
