<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class cardstatement extends Model
{
    //
     protected $table = 'cardstatement';
     protected $primarykey = 'transno';
     public $timestamps = false;
}
