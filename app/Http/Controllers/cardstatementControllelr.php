<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\cardstatement as card;

class cardstatementControllelr extends Controller
{
    //
     public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        return view('cardstatement');
    }

     public function insertcard()
    {
        return view('insertcard');
    }

    public function getstatement($id){
    	//$id = $request->input('index');
    	$statement =   card::select('*')->where('uid',$id)->get();
    	return $statement;
    }


    public function update(Request $request){

        $sell = $request->input('sell');
        $product =  $request->input('product'); 
        $price = $request->input('price');
        $transno = $request->input('transno');
        card::where('transno', $transno)->update(['sellerno'=>$sell,'product'=>$product,'price'=>$price]);
        echo("success");
    }

    public function insert(Request $request){
		$date = $request->input('date');
		$sell = $request->input('sell');
		$product = $request->input('product');
		$price = $request->input('price');
		$uid =$request->input('uid');
		$number = $request->input('number');

		card::insert(['date'=>$date, 'sellerno'=>$sell, 'product'=>$product, 'price'=>$price, 'number'=>$number, 'uid'=>$uid]);
		echo("Insert Success");
	
	}

}
