<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

use App\cardInfo as card;

class cardInfoControllelr extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

     public function index()
    {
        return view('cardinfo');
    }
    
    public function getcard(){
    	if(session('id')!=1){
    		$cinfo =   card::select('*')->where('uid',session('id'))->get();
    		
    		return $cinfo;
    	}else{
    		$cinfo = card::all();
    		return $cinfo;
    	}
    	
    }
}
