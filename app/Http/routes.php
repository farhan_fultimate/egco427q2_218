<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('auth.login');
});

Route::auth();

Route::get('/home', 'HomeController@index');

Route::get('/searchnews', 'searchControllelr@index');

Route::get('/cardinfo', 'cardInfoControllelr@index');
Route::get('/getcard','cardInfoControllelr@getcard');

Route::get('/cardstatement','cardstatementControllelr@index');
Route::get('/insertcard','cardstatementControllelr@insertcard');
Route::get('/getstatement/{id}','cardstatementControllelr@getstatement');
Route::post('/cardupdate','cardstatementControllelr@update');
Route::post('/cardinsert','cardstatementControllelr@insert');

Route::get('/transaction','transactionControllelr@index');
Route::get('/gettransaction','transactionControllelr@gettransaction');


