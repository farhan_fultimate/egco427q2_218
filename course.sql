-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: May 30, 2016 at 01:36 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.5.35

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `course`
--

-- --------------------------------------------------------

--
-- Table structure for table `cardinfo`
--

CREATE TABLE `cardinfo` (
  `name` varchar(25) NOT NULL,
  `number` varchar(25) NOT NULL,
  `issuer` varchar(25) NOT NULL,
  `exp` varchar(10) NOT NULL,
  `limit` int(11) NOT NULL,
  `currency` varchar(5) NOT NULL,
  `uid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cardinfo`
--

INSERT INTO `cardinfo` (`name`, `number`, `issuer`, `exp`, `limit`, `currency`, `uid`) VALUES
('John Doe', '4019 2445 0277 5567', 'Green Bank', '01/18', 5000, 'TH', 4),
('Jane Doe', '4019 3445 0277 5568', 'Blue Bank', '02/20', 70000, 'TH', 5),
('Bob Cat', '1234 5678 1234 5678', 'Green Bank', '12/18', 50000, 'TH', 7),
('Mary Lamb', '1122 3344 5566 7788', 'Red Bank', '10/18', 50000, 'THB', 6);

-- --------------------------------------------------------

--
-- Table structure for table `cardstatement`
--

CREATE TABLE `cardstatement` (
  `date` date NOT NULL,
  `sellerno` varchar(25) NOT NULL,
  `product` varchar(25) NOT NULL,
  `price` float NOT NULL,
  `number` varchar(25) NOT NULL,
  `uid` int(11) NOT NULL,
  `transno` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `cardstatement`
--

INSERT INTO `cardstatement` (`date`, `sellerno`, `product`, `price`, `number`, `uid`, `transno`) VALUES
('2013-02-01', 'F002', 'EGCI Cafe', 300, '4019 2445 0277 5567', 4, 1),
('2013-02-01', 'P001', 'Tablet', 20002, '4019 2445 0277 5567', 4, 2),
('2013-03-01', 'C001', 'The Mall', 2200, '4019 2445 0277 5567', 4, 3),
('2013-02-04', 'S002', 'Food Court', 670, '4019 3445 0277 5568', 5, 4),
('2013-02-02', 'C002', 'Smartphone', 18000, '4019 3445 0277 5568', 5, 5),
('2013-03-01', 'D001', 'The Mall', 3100, '4019 3445 0277 5568', 5, 6),
('2013-03-03', 'D002', 'ETC', 700, '1234 5678 1234 5678', 7, 7),
('2013-03-03', 'D003', 'ETC', 850, '1234 5678 1234 5678', 7, 8),
('2015-02-11', 'C005', 'IPad Air', 20000, '2345 1111 2222 3333', 3, 9),
('2015-02-09', 'Orange', 'CPad', 10001, '3377 4422 6699 1122', 4, 10),
('2015-02-12', 'Banana', 'BPad', 1, '1122 4455 6677 8811', 1, 11),
('2015-02-09', 'Pineapple', 'EPad', 500, '5522 9933 7744 6666', 1, 12),
('2015-02-11', '23', 'test', 1, '323', 1, 13),
('0000-00-00', 'TestNo', 'IPHONE', 1000, '78 3845483 438', 1, 14),
('0000-00-00', 'kfmglkj', 'gkjldfkj', 232, '353 43959', 3, 15),
('0000-00-00', 'sdlkfj', 'kgjroi', 8423, '54 3945839 8 43', 2, 16);

-- --------------------------------------------------------

--
-- Table structure for table `catalogue`
--

CREATE TABLE `catalogue` (
  `cid` int(11) NOT NULL,
  `catalogue_name` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='catalogue';

--
-- Dumping data for table `catalogue`
--

INSERT INTO `catalogue` (`cid`, `catalogue_name`) VALUES
(1, 'Action'),
(2, 'Drama'),
(3, 'Animation');

-- --------------------------------------------------------

--
-- Table structure for table `egciusers`
--

CREATE TABLE `egciusers` (
  `id` int(11) UNSIGNED NOT NULL,
  `name` varchar(20) NOT NULL,
  `username` varchar(20) NOT NULL,
  `password` varchar(50) NOT NULL,
  `email` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `egciusers`
--

INSERT INTO `egciusers` (`id`, `name`, `username`, `password`, `email`) VALUES
(1, 'Admin', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@localhost'),
(2, 'Amie', 'amie', 'e6a4370aca6970175dee8c72cc7e08dc', 'amie@localhost'),
(4, 'John', 'john', '527bd5b5d689e2c32ae974c6229ff785', 'john@localhost'),
(5, 'Jane', 'jane', '5844a15e76563fedd11840fd6f40ea7b', 'jane@localhost'),
(6, 'Mary', 'mary', 'b8e7be5dfa2ce0714d21dcfc7d72382c', 'mary@localhost'),
(7, 'Bob', 'bob', '9f9d51bc70ef21ca5c14f307980a29d8', 'bob@localhost'),
(3, 'Jim', 'jim', '5e027396789a18c37aeda616e3d7991b', 'jim@localhost'),
(9, 'ab', 'ab', '187ef4436122d1cc2f40dc2b92f0eba0', 'Zaba@Zaba.com'),
(10, '', '', '', ''),
(11, '', 'hibiki', '827ccb0eea8a706c4c34a16891f84e7b', 'hi@biki.me'),
(12, 'time', 'time', '', 'time_35@hotmail.com'),
(13, '', 'joonk', '3d5358ae37144456fd9d4cd17ebdd7b3', 'joonk@jok.juk'),
(14, 'C C', 'meenchawit', 'b2f5ff47436671b6e533d8dc3614845d', 'fake@mail.com'),
(15, 'Lana', 'La', '6ec66e124fb93c190e9207b0b82f542a', 'lana@example.com'),
(16, 'tappasarn', 'time723', 'bbf1e1103f7937f63247e193613ee6fc', 'time_345@hotmail.com'),
(17, 'robroo', 'robroo', 'a5a1a27ea8fe746fa7020348838ae384', 'robroo@robroo.com'),
(18, 'propr', 'propracong', '3043a42c5409d7557563556e9365f39e', 'proprac@gmail.com'),
(19, '', 'boss', 'ceb8447cc4ab78d2ec34cd9f11e4bed2', 'boss@smth.com'),
(20, 'testimony ', 'tester', 'e358efa489f58062f10dd7316b65649e', 'test@test.test'),
(21, '', 'tae', '2d5486b4c0f962f1cdaad61790fad438', 'tae@tae.tae'),
(22, 'TAE', 'tae16118', 'e3c7c3d0c547e193ad84a69f5d373e8a', 'taex@tae.tae');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `movie`
--

CREATE TABLE `movie` (
  `mid` varchar(6) NOT NULL,
  `cid` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `picture` varchar(50) NOT NULL,
  `coverpic` varchar(50) NOT NULL,
  `plot` varchar(2000) NOT NULL,
  `stock` int(11) NOT NULL,
  `price` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains item detail';

--
-- Dumping data for table `movie`
--

INSERT INTO `movie` (`mid`, `cid`, `title`, `picture`, `coverpic`, `plot`, `stock`, `price`) VALUES
('ac0001', 1, 'Avartar', 'web/images/cover_pic/avatar_movie.jpg', 'web/images/poster_pic/avatar-poster.jpg', 'When his brother is killed in a robbery, paraplegic Marine Jake Sully decides to take his place in a mission on the distant world of Pandora. There he learns of greedy corporate figurehead Parker Selfridge''s intentions of driving off the native humanoid "Na''vi" in order to mine for the precious material scattered throughout their rich woodland. In exchange for the spinal surgery that will fix his legs, Jake gathers intel for the cooperating military unit spearheaded by gung-ho Colonel Quaritch, while simultaneously attempting to infiltrate the Na''vi people with the use of an "avatar" identity. While Jake begins to bond with the native tribe and quickly falls in love with the beautiful alien Neytiri, the restless Colonel moves forward with his ruthless extermination tactics, forcing the soldier to take a stand - and fight back in an epic battle for the fate of Pandora.', 4, 4.99),
('ac0002', 1, 'Priest', 'web/images/cover_pic/Priest.jpg', 'web/images/poster_pic/priest-poster.jpg', 'PRIEST, a post-apocalyptic sci-fi thriller, is set in an alternate world -- one ravaged by centuries of war between man and vampires. The story revolves around a legendary Warrior Priest from the last Vampire War who now lives in obscurity among the other downtrodden human inhabitants in walled-in dystopian cities ruled by the Church. When his niece is abducted by a murderous pack of vampires, Priest breaks his sacred vows to venture out on a quest to find her before they turn her into one of them. He is joined on his crusade by his daughter''s boyfriend, a trigger-fingered young wasteland sheriff, and a former Warrior Priestess who possesses otherworldly fighting skills.', 2, 39.99),
('ac0003', 1, 'Unstoppable', 'web/images/cover_pic/Unstoppable.jpg', 'web/images/poster_pic/unstoppable-poster.jpg', 'Will Colson is starting a new job as a train conductor for the railroad. He is assigned to work with Frank Barnes a veteran train engineer. Initially there''s some tension between them. Later a train that is carrying chemicals is running with no one controlling it. It is feared that if it stops or crashes into a populated area it could disastrous. Connie, an exec suggests derailing it but the man in charge refuses to do that on account of what it will cost. When attempts to stop it fail, there''s fear of what they can do. But when Frank and Will have a close call with it, Frank decides to stop it themselves by coupling on to it. Will is not sure about it but sticks with Frank.', 2, 35.99),
('ac0004', 1, 'End Game', 'web/images/cover_pic/end-game.jpg', 'web/images/poster_pic/endgame-poster.jpg', 'A secret Service agent and a news reporter investigate the conspiracy behind the assassination of the President.', 3, 24.99),
('ac0005', 1, 'Wrath of the Titans', 'web/images/cover_pic/wrath-of-the-titan.jpg', 'web/images/poster_pic/wrath-titan-poster.jpg', 'A decade after his heroic defeat of the monstrous Kraken, Perseus-the demigod son of Zeus-is attempting to live a quieter life as a village fisherman and the sole parent to his 10-year old son, Helius. Meanwhile, a struggle for supremacy rages between the gods and the Titans. Dangerously weakened by humanity''s lack of devotion, the gods are losing control of the imprisoned Titans and their ferocious leader, Kronos, father of the long-ruling brothers Zeus, Hades and Poseidon. The triumvirate had overthrown their powerful father long ago, leaving him to rot in the gloomy abyss of Tartarus, a dungeon that lies deep within the cavernous underworld. Perseus cannot ignore his true calling when Hades, along with Zeus'' godly son, Ares (Edgar Ramírez), switch loyalty and make a deal with Kronos to capture Zeus. The Titans'' strength grows stronger as Zeus'' remaining godly powers are siphoned, and hell is unleashed on earth. Enlisting the help of the warrior Queen Andromeda (Rosamund Pike), Poseidon''s demigod son, Argenor (Toby Kebbell), and fallen god Hephaestus (Bill Nighy), Perseus bravely embarks on a treacherous quest into the underworld to rescue Zeus, overthrow the Titans and save mankind.', 5, 45.99),
('an0001', 3, 'Coraline', 'web/images/cover_pic/Coraline.jpg', 'web/images/poster_pic/coraline-poster.jpg', 'When Coraline moves to an old house, she feels bored and neglected by her parents. She finds a hidden door with a bricked up passage. During the night, she crosses the passage and finds a parallel world where everybody has buttons instead of eyes, with caring parents and all her dreams coming true. When the Other Mother invites Coraline to stay in her world forever, the girl refuses and finds that the alternate reality where she is trapped is only a trick to lure her.', 2, 29.99),
('an0002', 3, 'The Croods', 'web/images/cover_pic/Croods.jpg', 'web/images/poster_pic/the-croods-poster.jpg', 'The Croods is a prehistoric comedy adventure that follows the world''s first family as they embark on a journey of a lifetime when the cave that has always shielded them from danger is destroyed. Traveling across a spectacular landscape, the Croods discover an incredible new world filled with fantastic creatures -- and their outlook is changed forever.', 3, 44.59),
('an0003', 3, 'Mass Effect', 'web/images/cover_pic/mass-effect.jpg', 'web/images/poster_pic/mass-effect-poster.jpg', 'Following the conclusion of the first Mass Effect, Shepard and his ship, the Normandy, are dispatched to wipe out any further geth resistance. During the patrol in space, the Normandy is attacked by an unknown ship. Most of the crew escapes the Normandy, but Shepard is killed. Fortunately, Shepard''s body is retrieved and is revived by Cerberus, a pro-human organization that is at odds with the Alliance, whom Shepard was a part of. Two years later, Shepard learns from the Illusive Man, the leader of Cerberus, that numerous human colonies are vanishing, but the latter does not know why. After Shepard visits a colony that was recently attacked by these unknown belligerents, he learns that the belligerents are in fact Collectors, the same species that was behind the downing of the Normandy. Attacking the Collectors home world would be suicide. In response to this growing threat posed by the Collectors, Shepard creates a team made up of former squad members from the first Mass Effect was well as of new recruits. However, Shepard also must make the proper preparations necessary to confront the Collectors with the fewest casualties possible.', 3, 29.99),
('an0004', 3, 'Shrek', 'web/images/cover_pic/Shrek.jpg', 'web/images/poster_pic/shrek-poster.jpg', 'When a green ogre named Shrek discovers his swamp has been ''swamped'' with all sorts of fairytale creatures by the scheming Lord Farquaad, Shrek sets out with a very loud donkey by his side to ''persuade'' Farquaad to give Shrek his swamp back. Instead, a deal is made. Farquaad, who wants to become the King, sends Shrek to rescue Princess Fiona, who is awaiting her true love in a tower guarded by a fire-breathing dragon. But once they head back with Fiona, it starts to become apparent that not only does Shrek, an ugly ogre, begin to fall in love with the lovely princess, but Fiona is also hiding a huge secret.', 4, 29.99),
('an0005', 3, 'Toy Story', 'web/images/cover_pic/toy-story.jpg', 'web/images/poster_pic/toy-story-poster.jpg', 'A little boy named Andy loves to be in his room, playing with his toys, especially his doll named "Woody". But, what do the toys do when Andy is not with them, they come to life. Woody believes that he has life (as a toy) good. However, he must worry about Andy''s family moving, and what Woody does not know is about Andy''s birthday party. Woody does not realize that Andy''s mother gave him an action figure known as Buzz Lightyear, who does not believe that he is a toy, and quickly becomes Andy''s new favorite toy. Woody, who is now consumed with jealousy, tries to get rid of Buzz. Then, both Woody and Buzz are now lost. They must find a way to get back to Andy before he moves without them, but they will have to pass through a ruthless toy killer, Sid Phillips.', 3, 39.99),
('dr0001', 2, 'The Twilight: New Moon', 'web/images/cover_pic/New-Moon.jpg', 'web/images/poster_pic/new-moon-poster.jpg', 'After Bella recovers from the vampire attack that almost claimed her life, she looks to celebrate her birthday with Edward and his family. However, a minor accident during the festivities results in Bella''s blood being shed, a sight that proves too intense for the Cullens, who decide to leave the town of Forks, Washington for Bella and Edward''s sake. Initially heartbroken, Bella finds a form of comfort in reckless living, as well as an even-closer friendship with Jacob Black. Danger in different forms awaits.', 8, 49.99),
('dr0002', 2, 'The Twilight: Eclipse', 'web/images/cover_pic/Eclipse.jpg', 'web/images/poster_pic/eclipse-poster.jpg', 'Bella once again finds herself surrounded by danger as Seattle is ravaged by a string of mysterious killings and a malicious vampire continues her quest for revenge. In the midst of it all, she is forced to choose between her love for Edward and her friendship with Jacob -- knowing that her decision has the potential to ignite the struggle between vampire and werewolf. With her graduation quickly approaching, Bella is confronted with the most important decision of her life.', 5, 39.99),
('dr0003', 2, 'Black Swan', 'web/images/cover_pic/black-swan.jpg', 'web/images/poster_pic/black-swan-poster.jpg', 'Nina (Portman) is a ballerina in a New York City ballet company whose life, like all those in her profession, is completely consumed with dance. She lives with her obsessive former ballerina mother Erica (Hershey) who exerts a suffocating control over her. When artistic director Thomas Leroy (Cassel) decides to replace prima ballerina Beth MacIntyre (Ryder) for the opening production of their new season, Swan Lake, Nina is his first choice. But Nina has competition: a new dancer, Lily (Kunis), who impresses Leroy as well. Swan Lake requires a dancer who can play both the White Swan with innocence and grace, and the Black Swan, who represents guile and sensuality. Nina fits the White Swan role perfectly but Lily is the personification of the Black Swan. As the two young dancers expand their rivalry into a twisted friendship, Nina begins to get more in touch with her dark side - a recklessness that threatens to destroy her.', 8, 44.99),
('dr0004', 2, 'Titanic', 'web/images/cover_pic/titanic.jpg', 'web/images/poster_pic/Titanic-1997-poster.jpg', '84 years later, a 101-year-old woman named Rose DeWitt Bukater tells the story to her granddaughter Lizzy Calvert, Brock Lovett, Lewis Bodine, Bobby Buell and Anatoly Mikailavich on the Keldysh about her life set in April 10th 1912, on a ship called Titanic when young Rose boards the departing ship with the upper-class passengers and her mother, Ruth DeWitt Bukater, and her fiancé, Caledon Hockley. Meanwhile, a drifter and artist named Jack Dawson and his best friend Fabrizio De Rossi win third-class tickets to the ship in a game. And she explains the whole story from departure until the death of Titanic on its first and last voyage April 15th, 1912 at 2:20 in the morning.', 4, 59.99),
('dr0005', 2, 'Forrest Gump', 'web/images/cover_pic/forrest-gump.jpg', 'web/images/poster_pic/forrest-gump-poster.jpg', 'Forrest Gump is a simple man with a low I.Q. but good intentions. He is running through childhood with his best and only friend Jenny. His ''mama'' teaches him the ways of life and leaves him to choose his destiny. Forrest joins the army for service in Vietnam, finding new friends called Dan and Bubba, he wins medals, creates a famous shrimp fishing fleet, inspires people to jog, starts a ping-pong craze, create the smiley, write bumper stickers and songs, donating to people and meeting the president several times. However, this is all irrelevant to Forrest who can only think of his childhood sweetheart Jenny Curran. Who has messed up her life. Although in the end all he wants to prove is that anyone can love anyone.', 3, 54.99);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `personinfo`
--

CREATE TABLE `personinfo` (
  `id` int(11) NOT NULL,
  `firstname` varchar(30) NOT NULL,
  `lastname` varchar(30) NOT NULL,
  `city` varchar(20) NOT NULL,
  `country` varchar(20) NOT NULL,
  `telephone` varchar(20) NOT NULL,
  `email` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `personinfo`
--

INSERT INTO `personinfo` (`id`, `firstname`, `lastname`, `city`, `country`, `telephone`, `email`) VALUES
(4, 'John', 'Doe', 'LA', 'USA', '111-1111', 'john.doe@person.com'),
(5, 'Jane', 'Doe', 'NY', 'USA', '222-2222', 'jane.doe@person.com'),
(6, 'Mary', 'Lamb', 'BK', 'Thailand', '333-3333', 'mary.lamb@person.com'),
(7, 'Bob', 'Cat', 'BK', 'Thaliand', '444-4444', 'bob.cat@person.com'),
(1, 'admin', 'admin', 'admin', 'admin', '111', 'admin@admin.com');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `tid` int(11) NOT NULL,
  `uid` int(11) NOT NULL,
  `amount` varchar(20) NOT NULL,
  `date` datetime NOT NULL,
  `creditcard` varchar(25) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Keep purchasing history';

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`tid`, `uid`, `amount`, `date`, `creditcard`) VALUES
(1, 2, '154.97', '2015-05-05 05:44:12', '6689 5567 1436 8097'),
(2, 2, '154.97', '2015-05-05 05:52:58', '6689 5567 1436 8097');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `uid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `username` varchar(25) NOT NULL,
  `password` varchar(40) NOT NULL,
  `email` varchar(25) NOT NULL,
  `creditcard` varchar(25) NOT NULL,
  `accountType` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='Contains user account';

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`uid`, `name`, `username`, `password`, `email`, `creditcard`, `accountType`) VALUES
(1, 'Administration', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin@moviewarehouse.com', '', 2),
(2, 'Nat Cha', 'test', '81dc9bdb52d04dc20036dbd8313ed055', 'natcha@hotmail.com', '6689 5567 1436 8097', 1);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin@admin.com', '$2y$10$Fh3jwtJmkImSJzHhAzmYuO/8r0REAC06atU7Ub3jFynOgBWd2N/O6', 'oSUPIljRKiZxKRDpYnG6xLAqNx4OOdDHzSDgofXXqekEhIsFX53CZt5CHnmR', '2016-05-29 13:50:58', '2016-05-29 22:42:54'),
(2, 'amie', 'amie@admin.com', '$2y$10$HMhNl8UdoNhJ1T0qldtJ/uguYz0AAnn7rMzkeGOj5uXkqUFkYqjGu', 'KPiLWz9lNIZUEkbqEK2SzbTm64gRDwDREjqpvglIAMybTMoivK1GqQJPjxcZ', '2016-05-29 13:52:02', '2016-05-29 13:52:06'),
(3, 'jim', 'jim@admin.com', '$2y$10$Tp3/L82yv4PdsV.FshOn2Oo.LMngWGb.SByf3JzNJsiEgym.GePH.', 'vhAvMW4fPqrY1gwnzi60sVpfKlsAlfax31fGcDXUYu3SBN3dzwb2jLAKRO36', '2016-05-29 13:52:24', '2016-05-29 13:52:29'),
(4, 'john', 'john@admin.com', '$2y$10$My1Dqc2iXxKB5ZVlFLneTOl7zRgm00xMZPxnPnq5irxVh9SQzaTcO', 'X34BlRPijvNOw8X6hfNGzHytvIEz19ElhLiMiNTqJbHSibGIDj3yRLtro3vU', '2016-05-29 13:52:53', '2016-05-29 13:52:58');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cardstatement`
--
ALTER TABLE `cardstatement`
  ADD PRIMARY KEY (`transno`);

--
-- Indexes for table `catalogue`
--
ALTER TABLE `catalogue`
  ADD PRIMARY KEY (`cid`);

--
-- Indexes for table `egciusers`
--
ALTER TABLE `egciusers`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `movie`
--
ALTER TABLE `movie`
  ADD PRIMARY KEY (`mid`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`tid`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`uid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `email` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cardstatement`
--
ALTER TABLE `cardstatement`
  MODIFY `transno` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `catalogue`
--
ALTER TABLE `catalogue`
  MODIFY `cid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `egciusers`
--
ALTER TABLE `egciusers`
  MODIFY `id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `tid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `uid` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
